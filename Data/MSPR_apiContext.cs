﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MSPR_api.Models;

namespace MSPR_api.Data
{
    public class MSPR_apiContext : DbContext
    {
        public MSPR_apiContext (DbContextOptions<MSPR_apiContext> options)
            : base(options)
        {
        }

        public DbSet<MSPR_api.Models.User> User { get; set; }
    }
}
