﻿using MSPR_api.Models;
using System.Linq;

namespace MSPR_api.Data {

    public class Initializer {
    
        public static void Initialize(MSPR_apiContext context) {
        
            context.Database.EnsureCreated();

            if (context.User.Any()) {
                return;   // DB has been seeded
            }

            var seeders = new User[] {
                new User{FirstMidName="Carson",LastName="Alexander"},
                new User{FirstMidName="Meredith",LastName="Alonso"},
                new User{FirstMidName="Arturo",LastName="Anand"},
                new User{FirstMidName="Gytis",LastName="Barzdukas"},
                new User{FirstMidName="Yan",LastName="Li"},
                new User{FirstMidName="Peggy",LastName="Justice"},
                new User{FirstMidName="Laura",LastName="Norman"},
                new User{FirstMidName="Nino",LastName="Olivetto"}
            };

            foreach (User seeder in seeders) {
                context.User.Add(seeder);
            }
            context.SaveChanges();
        }
    }
}
